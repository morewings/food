$(document).ready(function(){
    var sliderWrap = $('.wrapper'),
        sliderTrigger = $('.send .button'),
        marker = $('.count').find('li').first().addClass('active');

    sliderWrap.find('li').first().attr('data-active', 'true');

    function moveMarker(){
        marker = marker.removeClass('active').next('li').addClass('active');
    }

    sliderTrigger.on('click', function(e){
        e.preventDefault();
       var  target = $('[data-active=true]'),
           input= target.find('input');
       if(input.is(":checked")){
           target.fadeOut('slow', moveMarker).removeAttr('data-active').next('li').attr('data-active', 'true');
       }
       else{
           alert('Nothing is checked!');
       }
    })

});