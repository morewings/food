<?php require 'parts/_header.php' ?>
<header>
  <h2>
    <span>מקצועני</span>
    <span class="accent">הבשר</span>
  </h2>
</header>
<div class="quiz group">
    <form action="#">
      <ul class="unstyled group count">
        <li>1</li>
        <li>2</li>
        <li>3</li>
        <li>4</li>
        <li>5</li>
        <li>6</li>
        <li>7</li>
        <li>8</li>
        <li>9</li>
      </ul>
      <ul class="group wrapper unstyled"> <!--Ul is for js that will be here-->
        <li>
          <div class="picture">
            <img src="/img/meat.jpg" alt="meat" width="480" height="290">
          </div>
          <div class="question">
            <h3>שם בעברית לסינטה</h3>

            <div class="control group">
              <input type="radio" id="a-1" name="q-1" value="1">
              <label for="a-1">מותן</label>
            </div>
            <div class="control group">
              <input type="radio" id="a-2" name="q-1" value="2">
              <label for="a-2">אילתית</label>
            </div>
            <div class="control group">
              <input type="radio" id="a-3" name="q-1" value="3">
              <label for="a-3">טיבון</label>
            </div>
          </div><!--/.question-->
        </li>
        <li>
          <div class="picture">
            <iframe width="480" height="290" src="http://www.youtube.com/embed/hqbVRhda8x8" frameborder="0" allowfullscreen></iframe>
          </div>
          <div class="question">
            <h3>לפי הסרטון, כמה שמן צריך לשפוך במחבת כדי להכין קציצות? </h3>

            <div class="control group">
              <input type="radio" id="b-1" name="q-2" value="1">
              <label for="b-1">מותן</label>
            </div>
            <div class="control group">
              <input type="radio" id="b-2" name="q-2" value="2">
              <label for="b-2">אילתית</label>
            </div>
            <div class="control group">
              <input type="radio" id="b-3" name="q-2" value="3">
              <label for="b-3">מספיק כדי שתכסה את החלק התחתון של הקציצה</label>
            </div>
          </div><!--/.question-->
        </li>
      </ul>
      <div class="send">
        <a class="button" href="#">
          <span>לשאלה הבאה</span>
          <span class="accent">&gt;&gt;</span>
        </a>
      </div>
    </form>

</div>
<?php require 'parts/_footer.php' ?>