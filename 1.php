<?php require 'parts/_header.php' ?>
<header>
  <h2>
    <span>מקצועני</span>
    <span class="accent">הבשר</span>
  </h2>
</header>
<div class="first-page">
  <div class="inner-box">
    <h2>
      <span>רוצים ללמוד</span>
      <span class="accent">עוד על בשר?</span>
    </h2>
    <p>
      <strong>בואו לקחת חלק באחת מסדנאות בישול בשר מתנת אדום אדום ברחבי הארץ!</strong> ענו נכונה על כל השאלות, כתבו לנו איזו תוספת הכי מתאימה לדעתכם למנת בשר ולמה? ואולי תזכו להשתתף באחת מסדנאות בישול הבשר. נתראה שם!
    </p>
    <a class="button" href="#">התחילו את השאלון <span class="accent">&gt;&gt;</span></a>
  </div>

</div>
<?php require 'parts/_footer.php' ?>